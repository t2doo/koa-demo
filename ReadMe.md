## 一. 项目的初始化
### 1. npm 初始化
```bash
npm init -y
```
生成 `package.jso` 文件
- 记录项目的依赖

### 2. git 初始化
```bash
git init
```
生成 `.git` 隐藏文件夹, git 的本地仓库

### 3. 创建 ReadMe 文件

### 4. 创建 `.gitignore` 用以忽略掉不想 git 提交的文件或文件夹

## 二. 搭建项目
### 1. 安装 Koa 框架
```bash
npm install koa
```
### 2. 编写最基本的框架 app
创建 `src/main.js`
```javascript
const Koa = require('koa')
const app = new Koa()

app.use((ctx, next) => {
  ctx.body = 'hello api'
})

app.listen('3000', () => {
  console.log('server is running on http://localhost:3000')
})
```
### 3. 测试
```bash
node src/main
```
## 三. 项目的基本优化
### 1. 自动重启服务
安装 `nodemon` 工具
```bash
npm install nodemon
```
编写 `package.json` 启动命令
```json
"scripts": {
    "dev": "nodemaon ./src/main.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  }
```
执行 `npm run dev` 启动服务
### 读取配置文件
安装 `dotenv`, 读取根目录中的 `.env` 文件, 将配置写到 `process.env` 中
```bash
npm insall dotenv
```
创建 `.env` 文件
```env
APP_PORT=8000
```
创建 `src/config/config.default.js`
```javascript
const dotenv = require('dotenv')

dotenv.config()

module.exports = process.env
```
编辑 `main.js`
```javascript
const Koa = require('koa')

const { APP_PORT } = require('./config/config.default')

const app = new Koa()

app.use((ctx, next) => {
  ctx.body = 'hello api'
})

app.listen(APP_PORT, () => {
  console.log(`server is running on http://localhost:${APP_PORT}`)
})

```
## 四. 添加路由
路由: 根据不同的URL, 调用对应处理函数
### 1. 安装 koa-router
```bash
npm install koa-router
```
步骤:
1. 导入包
2. 实例化对象
3. 编写路由
4. 注册中间件
### 2. 编写路由
创建 `src/router` 目录, 编写 `user.route.js`
```javascript
const Router = require('koa-router')

const router = new Router({
    prefix: '/users'
})

router.get('/', (ctx, next) => {
    ctx.body = 'hello users'
})

module.exports = router
```
### 3. 编辑 `main.js`
```javascript
const userRouter = require('./router/user.route')
app.use(userRouter.routes())
```
## 五. 目录结构优化
1. 将 http 目录和 app 业务拆分
- 创建 `src/app/index.js`
```javascript
const Koa = require('koa')

const userRouter = require('../router/user.route')

const app = new Koa()

app.use(userRouter.routes())

module.exports = app
```
2. 编辑 `main.js`
```javascript
const { APP_PORT } = require('./config/config.default')

const app = require('./app')

app.listen(APP_PORT, () => {
  console.log(`server is running on http://localhost:${APP_PORT}`)
})
```
2. 将路由和控制器拆分
- 路由: 解析 URL, 分发给控制器对应的方法
- 控制器: 处理不同的业务
- 编辑 `user.route.js`
```javascript
const Router = require('koa-router')

const { register, login } = require('../controller/user.controller')

const router = new Router({
  prefix: '/users'
})

router.post('/register', register)
router.post('/login', login)

module.exports = router
```
创建 `controller/user.controller.js`
```javascript
class UserController {
  async register (ctx, next) {
    ctx.body = '用户注册成功'
  }

  async login (ctx, next) {
    ctx.body = '登录成功'
  }
}

module.exports = new UserController()
```
## 六. 解析 body
### 1. 安装 koa-body
```bash
npm install koa-body
```
### 2. 注册中间件
编辑 `app/index.js`
```javascript
const KoaBody = require('koa-body')

app.use(KoaBody())
app.use(userRouter.routes())
```
### 3. 解析请求数据
编辑 `user.controller.js`
```javascript
const { createUser } = require('../service/user.service')

class UserController {
  async register (ctx, next) {
    // 1. 获取数据
    // console.log(ctx.request.body)
    const { user_name, password } = ctx.request.body
    // 2. 操作数据库
    const res = await createUser(user_name, password)
    console.log(res)
    // 3. 返回结果
    ctx.body = ctx.request.body
  }

  async login (ctx, next) {
    ctx.body = '登录成功'
  }
}

module.exports = new UserController()
```
### 4. 拆分 `service` 层
service 层主要是做数据库处理
创建 `service/user.service.js`
```javascript
class UserService {
  async createUser (user_name, password) {
    return '写入数据库成功'
  }
}

module.exports = new UserService()
```
## 七. 数据库操作
sequelize ORM 数据库工具
ORM: 对象关系映射
- 数据表映射(对应)一个类
- 数据表中的数据行(记录)对应一个对象
- 数据表字段对应对象的属性
- 数据表的操作对应对象的方法
### 1. 安装 sequelize
```bash
npm install sequelize
```
### 2. 连接数据库
`src/db/seq.js`
```javascript
const { Sequelize } = require('sequelize')

const {
  MYSQL_HOST,
  MYSQL_PORT,
  MYSQL_USER,
  MYSQL_PWD,
  MYSQL_DB
} = require('../config/config.default')

const seq = new Sequelize(MYSQL_DB, MYSQL_USER, MYSQL_PWD, {
  host: MYSQL_HOST,
  dialect: 'mysql'
})

// seq.authenticate().then(() => {
//   console.log('数据库连接成功')
// }).catch(err => {
//   console.log('数据库连接失败', err)
// })

module.exports = seq
```
## 十一. 拆分中间件
## 十二. 加密
用加盐加密方法
### 1. 安装 bcrytjs
```bash
npm install bcryptjs
```
## 十三. 登录验证
## 十四. 用户的认证
 登录成功后, 给用户颁发一个令牌 token, 用户在以后的每一次请求中携带这个令牌
 jwt: jsonWebToken
 - header: 头部
 - payload: 载荷
 - signature: 签名
